(import (scheme base)
        (scheme file)
        (hyphenate pattern-trie))

(with-output-to-file "graph.gv"
  (lambda ()
    (pattern-trie-graph
     (with-input-from-file "../data/hyphenation.pat.txt"
       pattern-trie-parse))))
