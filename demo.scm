(import (scheme base)
        (scheme write)
        (scheme file)
        (hyphenate pattern-trie)
        (hyphenate hyphenator))

(cond-expand
 (guile (import (srfi srfi-1)))
 (chibi (import (srfi 1))))

(define patterns
  (with-input-from-file "data/hyph-en-us.pat.txt"
    pattern-trie-parse))

(define hyphenate
  (hyphenator patterns 2 3))

(display
 (append-map hyphenate '("word" "hyphenation" "by" "computer")))
(newline)
0
