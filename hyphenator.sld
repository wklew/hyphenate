;; Copyright (C) 2021, 2022 Walter Lewis
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (hyphenate hyphenator)

  (export hyphenator
          list-hyphenator
          list-scorer)

  (import (scheme base)
          (hyphenate pattern)
          (hyphenate pattern-trie))

  (begin

    (define (list-scorer trie left right)
      (lambda (cs)
        (let ((len (length cs)))
          (if (< len (+ left right))
              (make-list (+ 1 len) 0)
              (append
               (make-list left 0)
               (pattern-take (+ 1 (- len left right))
                             (pattern-drop left
                                           (pattern-trie-lookup cs trie))))))))

    (define (list-hyphenator trie left right)
      (let ((score (list-scorer trie left right)))
        (lambda (cs)
          (let loop ((acc '())
                     (cs cs)
                     (ps (cdr (score cs))))
            (if (null? ps)
                (list (append (reverse acc) cs))
                (let ((c (car cs)) (cs (cdr cs))
                      (p (car ps)) (ps (cdr ps)))
                  (if (odd? p)
                      (cons (reverse (cons c acc))
                            (loop '() cs ps))
                      (loop (cons c acc) cs ps))))))))

    (define (hyphenator trie left right)
      (let ((hyph (list-hyphenator trie left right)))
        (lambda (str)
          (map list->string
               (hyph (string->list str))))))))
