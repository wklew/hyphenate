;; Copyright (C) 2021, 2022 Walter Lewis
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (hyphenate pattern-trie)

  (export <pattern-trie>
          make-pattern-trie
          pattern-trie?
          pattern-trie-score
          pattern-trie-kids
          pattern-trie-empty
          pattern-trie-single
          pattern-trie-lookup
          pattern-trie-insert
          pattern-trie-parse
          pattern-trie->list
          pattern-trie-graph)

  (import (scheme base)
          (scheme write)
          (hyphenate pattern)
          (containers dictionary))

  (begin

    ;; Recursive trie of scored hyphenation patterns
    (define-record-type <pattern-trie>
      (make-pattern-trie score kids)
      pattern-trie?
      (score pattern-trie-score)
      (kids pattern-trie-kids))

    (define (pattern-trie-empty)
      "Construct the empty pattern trie."
      (make-pattern-trie '() (dictionary-empty)))

    (define (pattern-trie-single key val)
      "Construct a singleton pattern trie with KEY mapped to VAL."
      (make-pattern-trie '() (dictionary-single key val)))

    ;; Internal lookup helper
    (define (pattern-trie-lookup1 cs trie)
      (let ((ns (pattern-trie-score trie)))
        (if (null? cs)
            ns
            (guard (_ (else ns))
              (pattern-zip
               ns
               (pattern-trie-lookup1
                (cdr cs)
                (dictionary-lookup
                 (char->integer (car cs))
                 (pattern-trie-kids trie))))))))

    (define (pattern-trie-lookup cs trie)
      "Look up the score associated with pattern CS in pattern trie TRIE."
      (let ((ns (pattern-trie-score trie)))
        (pattern-unwrap
         (let loop ((cs (pattern-wrap #\. cs)))
           (if (null? cs)
               ns
               (pattern-zip
                (pattern-trie-lookup1 cs trie)
                (cons 0 (loop (cdr cs)))))))))

    ;; Internal insertion helper
    (define (pattern-trie-init ns cs)
      (let loop ((cs cs))
        (if (null? cs)
            (make-pattern-trie ns (dictionary-empty))
            (pattern-trie-single (char->integer (car cs))
                                 (loop (cdr cs))))))

    (define (pattern-trie-insert cs trie)
      "Insert pattern CS into pattern trie TRIE."
      (let-values (((ns cs) (pattern-unzip cs)))
        (let loop ((cs cs)
                   (trie trie))
          (if (null? cs)
              (make-pattern-trie ns (pattern-trie-kids trie))
              (let ((c (car cs))
                    (cs (cdr cs)))
                (make-pattern-trie
                 (pattern-trie-score trie)
                 (dictionary-insert (lambda (_ trie)
                                      (loop cs trie))
                                    (char->integer c)
                                    (pattern-trie-init ns cs)
                                    (pattern-trie-kids trie))))))))

    (define (pattern-trie-parse)
      "Read lines from the current input port and insert them as patterns into a
new pattern trie."
      (let loop ((trie (pattern-trie-empty)))
        (let ((str (read-line)))
          (if (eof-object? str)
              trie
              (loop
               (pattern-trie-insert (string->list str)
                                    trie))))))

    (define (pattern-trie->list trie)
      "Convert pattern trie TRIE to a more standard list representation."
      (cons (pattern-trie-score trie)
            (dictionary-fold-left
             (lambda (n trie lst)
               (cons (cons n (pattern-trie->list trie))
                     lst))
             '()
             (pattern-trie-kids trie))))

    (define (pattern-chars->string ns)
      (list->string (reverse ns)))

    (define (pattern-score->string ns)
      (apply string-append (map number->string ns)))

    (define (pattern-trie-graph trie)
      "Write pattern trie TRIE to a Graphviz graph using the current output port."
      (define (write-link source dest)
        (write source)
        (display " -> ")
        (write dest)
        (newline))
      (define (write-score name ns)
        (when (pair? ns)
          (let ((str (pattern-score->string ns)))
            (write str)
            (display " [shape=box,width=0,height=0.1]")
            (newline)
            (write-link name str))))
      (define (write-node name c)
        (write name)
        (display " [label=")
        (write (string c))
        (display "fixedsize=true,width=0.25,height=0.25,margin=0]")
        (newline))
      (define (loop cs trie)
        (let ((name (pattern-chars->string cs))
              (ns (pattern-trie-score trie)))
          (write-node name (car cs))
          (write-score name (pattern-trie-score trie))
          (dictionary-fold-left
           (lambda (n trie _)
             (let ((cs (cons (integer->char n) cs)))
               (write-link name (pattern-chars->string cs))
               (loop cs trie)))
           (if #f #f)
           (pattern-trie-kids trie))))
      (display "digraph Trie {")
      (newline)
      (write-score "root" (pattern-trie-score trie))
      (dictionary-fold-left
       (lambda (n trie _)
         (loop (list (integer->char n))
               trie))
       (if #f #f)
       (pattern-trie-kids trie))
      (display "}")
      (newline))))
