;; Copyright (C) 2021, 2022 Walter Lewis
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(import (scheme base)
        (hyphenate pattern))

(cond-expand
  (chibi (import (chibi test)))
  (guile (import (srfi srfi-64))))

;;; Tested against Haskell's Text.Hyphenation.Patterns

(define (test-pattern str1 ns str2)
  (test-assert (string-append "scoring \"" str1 "\"")
    (equal? (cons ns str2)
            (let-values (((ns ls) (pattern-unzip (string->list str1))))
              (cons ns (list->string ls))))))

(test-begin "pattern")

(test-pattern "" '(0) "")

(for-each (lambda (str ns)
            (test-pattern str ns ""))
          '("1" "2" "3" "4" "5")
          '((1) (2) (3) (4) (5)))

(test-pattern "hy3ph" '(0 0 3 0 0) "hyph")
(test-pattern "he2n" '(0 0 2 0) "hen")
(test-pattern "hena4" '(0 0 0 0 4) "hena")
(test-pattern "hen5at" '(0 0 0 5 0 0) "henat")
(test-pattern "1na" '(1 0 0) "na")
(test-pattern "n2at" '(0 2 0 0) "nat")
(test-pattern "1tio" '(1 0 0 0) "tio")
(test-pattern "2io" '(2 0 0) "io")
(test-pattern "hy3phe2n5a4t2ion"
              '(0 0 3 0 0 2 5 4 2 0 0 0)
              "hyphenation")

(test-end "pattern")
