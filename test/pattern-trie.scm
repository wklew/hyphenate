;; Copyright (C) 2021, 2022 Walter Lewis
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(import (scheme base)
        (hyphenate pattern-trie))

(cond-expand
  (chibi (import (chibi test)))
  (guile (import (srfi srfi-64))))

;;; Tested against Haskell's Text.Hyphenation.Patterns (with some
;;; hacking done to gain access to internals)

;;; Testing insertion by building up pattern tries containing
;;; progressively more patterns

(define (test-insert str trie tree)
  (let ((trie (pattern-trie-insert (string->list str)
                                   trie)))
    (test-assert (string-append "inserting \"" str "\"")
      (equal? tree
              (pattern-trie->list trie)))
    trie))

(test-begin "pattern-trie")

(test-begin "pattern-trie-insert")

;; p = mempty

(define trie0 (pattern-trie-empty))

;; p' = insertPattern "hy3ph" p

;; p'

;; Patterns [] (fromList [(104,Patterns [] (fromList [(121,Patterns [] (fromList [(112,Patterns [] (fromList [(104,Patterns [0,0,3,0,0] (fromList []))]))]))]))])

;; formatted to look like lisp:

;; Patterns []
;;   [(104 Patterns []
;;         [(121 Patterns []
;;               [(112 Patterns []
;;                     [(104 Patterns [0 0 3 0 0]
;;                           [])])])])]

(define trie1
  (test-insert
   "hy3ph"
   trie0
   '(()
     ;; H
     (104 ()
          ;; Y
          (121 ()
               ;; P
               (112 ()
                    ;; H
                    (104 (0 0 3 0 0))))))))

;; p'' = insertPattern "he2n" p'

;; p''

;; Patterns [] (fromList [(104,Patterns [] (fromList [(101,Patterns [] (fromList [(110 ,Patterns [0 ,0 ,2 ,0] (fromList []))])),(121,Patterns [] (fromList [(112,Patterns [] (fromList [(104,Patterns [0,0,3,0,0] (fromList []))]))]))]))])

;; Patterns []
;;   [(104 Patterns []
;;         [(101 Patterns []
;;               [(110 Patterns [0,0,2,0]
;;                     [])])
;;          (121 Patterns []
;;               [(112 Patterns []
;;                     [(104 Patterns [0,0,3,0,0]
;;                           [])])])])]

(define trie2
  (test-insert
   "he2n"
   trie1
   ;; equivalent to GHC results modulo sorting order
   '(()
     ;; H
     (104 ()
          ;; Y (shared)
          (121 ()
               ;; P
               (112 ()
                    ;; H
                    (104 (0 0 3 0 0))))
          ;; E (new)
          (101 ()
               ;; N
               (110 (0 0 2 0)))))))

;; p''' = insertPattern "hena4" p''

;; p'''

;; Patterns [] (fromList [(104,Patterns [] (fromList [(101,Patterns [] (fromList [(110,Patterns [0,0,2,0] (fromList [(97,Patterns [0,0,0,0,4] (fromList []))]))])),(121,Patterns [] (fromList [(112,Patterns [] (fromList [(104,Patterns [0,0,3,0,0] (fromList []))]))]))]))])

;; Patterns []
;;   [(104 Patterns []
;;         [(101 Patterns []
;;               [(110 Patterns [0 0 2 0]
;;                     [(97 Patterns [0 0 0 0 4]
;;                          [])])])
;;          (121 Patterns []
;;               [(112 Patterns []
;;                     [(104 Patterns [0 0 3 0 0] [])])])])]

(define trie3
  (test-insert
   "hena4"
   trie2
   ;; equivalent to GHC results modulo sorting order
   '(()
     ;; H
     (104 ()
          ;; Y
          (121 ()
               ;; P
               (112 ()
                    ;; H
                    (104 (0 0 3 0 0))))
          ;; E
          (101 ()
               ;; N
               (110 (0 0 2 0)
                    ;; A (new)
                    (97 (0 0 0 0 4))))))))

(test-end "pattern-trie-insert")

(test-begin "pattern-trie-lookup")

;;; Now that we have these different pattern tries, we can test lookup
;;; and sharing on each independently

(define (test-lookup str trie score)
  (test-assert (string-append "looking up \"" str "\"")
    (equal? score
            (pattern-trie-lookup (string->list str)
                                 trie))))

;; word = "hyphenation"

(define word "hyphenation")

;; lookupPattern word p

;; [0,0,0,0,0,0,0,0,0,0,0]

(test-lookup word trie0 '(0 0 0 0 0 0 0 0 0 0 0))

;; lookupPattern word p'

;; [0,0,3,0,0,0,0,0,0,0,0]

(test-lookup word trie1 '(0 0 3 0 0 0 0 0 0 0 0))

;; lookupPattern word p''

;; [0,0,3,0,0,2,0,0,0,0,0]

(test-lookup word trie2 '(0 0 3 0 0 2 0 0 0 0 0))

;; lookupPattern word p'''

;; [0,0,3,0,0,2,0,4,0,0,0]

(test-lookup word trie3 '(0 0 3 0 0 2 0 4 0 0 0))

(test-end "pattern-trie-lookup")

(test-end "pattern-trie")
