;; Copyright (C) 2021, 2022 Walter Lewis
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(import (scheme base)
        (scheme file)
        (hyphenate pattern-trie)
        (hyphenate hyphenator))

(cond-expand
  (chibi (import (chibi test)))
  (guile (import (srfi srfi-64))))

(define patterns
  (with-input-from-file "../data/hyph-en-us.pat.txt"
    pattern-trie-parse))

(define score
  (list-scorer patterns 2 3))

(define hyphenate
  (hyphenator patterns 2 3))

;;; Tested against Haskell's Text.Hyphenation.Hyphenator

(define (test-hyphen str scores strs)
  (test-assert (string-append "scoring \"" str "\"")
    (equal? scores
            (score (string->list str))))
  (test-assert (string-append "hyphenating \"" str "\"")
    (equal? strs
            (hyphenate str))))

(test-begin "hyphenate")

;; hyphenationScore english_US "computer"
;; [0,0,4,5,0,2]

;; hyphenate english_US "computer"
;; ["com","puter"]

(test-hyphen
 "computer"
 '(0 0 4 5 0 2)
 '("com" "puter"))

;; hyphenationScore english_US "hyphenation"
;; [0,0,3,0,0,2,5,4,2]

;; hyphenate english_US "hyphenation"
;; ["hy","phen","ation"]

(test-hyphen
 "hyphenation"
 '(0 0 3 0 0 2 5 4 2)
 '("hy" "phen" "ation"))

;; hyphenationScore english_US "supercalifragilisticexpialadocious"
;; [0,0,1,0,0,1,0,0,1,0,0,0,0,1,0,4,0,1,2,0,1,0,3,2,3,0,3,2,0,1,2,0]

;; hyphenate english_US "supercalifragilisticexpialadocious"
;; ["su","per","cal","ifrag","ilis","tic","ex","pi","al","ado","cious"]

(test-hyphen
 "supercalifragilisticexpialadocious"
 '(0 0 1 0 0 1 0 0 1 0 0 0 0 1 0 4 0 1 2 0 1 0 3 2 3 0 3 2 0 1 2 0)
 '("su" "per" "cal" "ifrag" "ilis" "tic" "ex" "pi" "al" "ado" "cious"))

(test-end "hyphenate")
