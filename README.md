# Hyphenate

R7RS Scheme library providing Knuth-Liang hyphenation, as used in TeX.  Ported
from the [Haskell library](https://hackage.haskell.org/package/hyphenation) by
Edward Kmett.

## Demo (Chibi)

```scheme
(import (scheme base)
        (srfi 1)
        (hyphenate pattern-trie)
        (hyphenate hyphenator))

(define patterns
  (with-input-from-file "data/hyph-en-us.pat.txt"
    pattern-trie-parse))

(define hyphenate (hyphenator patterns 2 3))

(append-map hyphenate '("word" "hyphenation" "by" "computer"))

; => ("word" "hy" "phen" "ation" "by" "com" "puter")
```

## Requirements

- SRFI-64
- [containers](https://git.sr.ht/~wklew/containers)

To run the tests you currently need Guile or Chibi Scheme.

## Documentation

WIP

## Graph

The `pattern-trie-graph` procedure from `(hyphenate pattern-trie)` is useful
to see what a pattern trie looks like, conceptually.  These graphs can be
compared with those from [Liang's thesis](https://www.tug.org/docs/liang/).

The graph below shows a small trie of patterns which match on the word
"hyphenation", with boxed sequences of numbers representing a hyphenation
score up to that point.

![graph of a pattern trie comprised of patterns matching the word "hyphenation"](graph/graph.png)
