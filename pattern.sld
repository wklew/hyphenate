;; Copyright (C) 2021, 2022 Walter Lewis
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (hyphenate pattern)

  (export pattern-unzip
          pattern-take
          pattern-drop
          pattern-wrap
          pattern-unwrap
          pattern-zip)

  (cond-expand
   (guile (import (srfi srfi-1)))
   (chibi (import (srfi 1))))

  (import (scheme base))

  (begin

    (define (pattern-unzip cs)
      "Split pattern CS into matching lists of letters and score numbers."
      (define (return xs ys)
        (values (reverse xs)
                (reverse ys)))
      (let loop ((ns '())
                 (ls '())
                 (cs cs))
        (if (null? cs)
            (return (cons 0 ns)
                    ls)
            (let ((c (car cs))
                  (cs (cdr cs)))
              (if (not (or (char<? c #\0) (char>? c #\9)))
                  (let ((ns (cons (string->number (string c))
                                  ns)))
                    (if (null? cs)
                        (return ns ls)
                        (loop ns
                              (cons (car cs) ls)
                              (cdr cs))))
                  (loop (cons 0 ns)
                        (cons c ls)
                        cs))))))

    (define (pattern-take n cs)
      "Take up to N elements from the beginning of pattern CS."
      (take cs n))

    (define (pattern-drop n cs)
      "Drop up to N elements from the beginning of pattern CS."
      (drop cs n))

    (define (pattern-wrap x xs)
      "Wrap pattern XS by inserting X at either end."
      (cons x (append xs (list x))))

    (define (pattern-unwrap xs)
      "Drop one element from either end of wrapped pattern XS."
      (reverse (cdr (reverse (cdr xs)))))

    (define (pattern-zip ns ms)
      "Merge pattern scores NS and MS to produce an element-wise maximum score."
      (cond
       ((null? ns) ms)
       ((null? ms) ns)
       (else
        (cons (max (car ns)
                   (car ms))
              (pattern-zip (cdr ns)
                           (cdr ms))))))))
